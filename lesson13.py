# # Циклы - while and for
# i = -5
# while i <= 10:
#     print(f'{i}', end=' \ ')
#     i += 1 # аналог записи i = i + 1
# print('Hello', 'world', sep='@', end=' | ')

# s = 'Hello world'
# for i in s:
#     # print(f'{i}', end = ' ')
#     if i == ' ': continue
#     print(f'"{i}"', end='  ')

# for i in 'Hello world':
#     if i == ",": break
#     print(i, end='     ')
# else:
#     print('\nNo Spaces')

#
# i = 1900
# while i <= 2022:
#     print(f'{i}', end=" ")
#     i += 1
